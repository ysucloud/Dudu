package com.ysucloud.exapis.repository;

import com.ysucloud.exapis.dataobject.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:25
 */
public interface CommentRepository extends JpaRepository<Comment,Integer> {

    List<Comment> findByProductId(Integer productId);

}
