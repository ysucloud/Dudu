package com.ysucloud.exapis.repository;

import com.ysucloud.exapis.dataobject.Message;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:25
 */
public interface MessageRepository extends JpaRepository<Message,Integer> {
}
