package com.ysucloud.exapis.repository;

import com.ysucloud.exapis.dataobject.BuyerInfo;
import com.ysucloud.exapis.dataobject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 15:13
 */
public interface BuyerInfoRepository extends JpaRepository<BuyerInfo,Integer>
{

}
