package com.ysucloud.exapis.repository;

import com.ysucloud.exapis.dataobject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 15:19
 */
public interface ProductInfoRepository extends JpaRepository<ProductInfo,Integer>
{
    List<ProductInfo> findByProductStatus(Integer productStatus);
    List<ProductInfo> findByProductIdIn(List<Integer> productIdList);
}
