package com.ysucloud.exapis.repository;

import com.ysucloud.exapis.dataobject.OrderMaster;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 15:17
 */
public interface OrderMasterRepository extends JpaRepository<OrderMaster,Integer>
{
    List<OrderMaster> findByBuyerId(Integer buyerId);

    OrderMaster findByBuyerIdAndOrderId(Integer buyerId,Integer orderId);
}
