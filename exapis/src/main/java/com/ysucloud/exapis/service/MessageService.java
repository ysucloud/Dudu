package com.ysucloud.exapis.service;

import com.ysucloud.exapis.dataobject.Message;

import java.util.List;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:26
 */
public interface MessageService {

    boolean save(String message);

    List<Message> findAll();

}
