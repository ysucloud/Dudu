package com.ysucloud.exapis.service.impl;

import com.ysucloud.exapis.dataobject.Message;
import com.ysucloud.exapis.repository.MessageRepository;
import com.ysucloud.exapis.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:30
 */
@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    MessageRepository messageRepository;

    @Override
    public boolean save(String message) {
        Message newMessage = new Message();

        newMessage.setMessage(message);

        messageRepository.save(newMessage);

        return true;
    }

    @Override
    public List<Message> findAll() {
        List<Message> messageList = messageRepository.findAll();

        return messageList;
    }
}
