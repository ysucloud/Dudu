package com.ysucloud.exapis.service.impl;

import com.ysucloud.exapis.dataobject.Comment;
import com.ysucloud.exapis.form.CommentForm;
import com.ysucloud.exapis.repository.CommentRepository;
import com.ysucloud.exapis.service.CommentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:43
 */
@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentRepository commentRepository;

    @Override
    public List<Comment> findAll(Integer productId) {
        List<Comment> commentList = commentRepository.findByProductId(productId);

        return commentList;
    }

    @Override
    public boolean save(CommentForm commentForm) {
        Comment comment = new Comment();

        BeanUtils.copyProperties(commentForm,comment);

        commentRepository.save(comment);
        return true;
    }
}
