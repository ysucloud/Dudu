package com.ysucloud.exapis.service;

import com.ysucloud.exapis.dataobject.Comment;
import com.ysucloud.exapis.form.CommentForm;

import java.util.List;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:39
 */
public interface CommentService {

    List<Comment> findAll(Integer productId);

    boolean save(CommentForm commentForm);
}
