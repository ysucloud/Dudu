package com.ysucloud.exapis.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.constraints.NotNull;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:41
 */
@Data
public class CommentForm {
    @NotNull
    private Integer buyerId;

    @NotNull
    private Integer productId;

    @NotEmpty
    private String message;
}
