package com.ysucloud.exapis.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 9:19
 */
@Entity
@Data
@Table(name = "order_detail")
public class OrderDetail {

    /** 无意义自增主键 */
    @Id
    @GeneratedValue
    private Integer detailId;

    /** 订单id（与order_master表关联）*/
    private Integer orderId;

    /** 商品ID （与product_info表关联） */
    private Integer productId;

    /** 商品名称 */
    private String productName;

    /** 商品单价 */
    private BigDecimal productPrice;

    /** 商品数量 */
    private Integer productQuantity;

    /** 商品小图 */
    private String productIcon;

    /** 自动时间戳*/
    private Date createTime;
    private Date updateTime;


    /** 增加冗余字段加速查询*/
}
