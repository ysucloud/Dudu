package com.ysucloud.exapis.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import java.util.Date;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:21
 */
@Entity
@DynamicInsert
@Data
@Table(name = "message")
public class Message {
    /** 自增主键 */
    @Id
    @GeneratedValue
    private Integer Id;

    /** 消息 */
    private String message;

    /** 创建时间 */
    private Date createTime;

    public Message(){

    }
}
