package com.ysucloud.exapis.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:16
 */
@Entity
@Data
@DynamicInsert
public class Comment {
    /** 评论主键 */
    @Id
    @GeneratedValue
    private Integer Id;

    /** 商品ID */
    private Integer productId;

    /** 买家ID */
    private Integer buyerId;

    /** 评论内容 */
    private String message;

    /** 自动时间戳 */
    private Date createTime;

}
