package com.ysucloud.exapis.controller;

import com.alibaba.fastjson.JSONObject;
import com.ysucloud.exapis.Utils.ResultVOUtil;
import com.ysucloud.exapis.VO.ResultVO;
import com.ysucloud.exapis.exception.NullParameterException;
import com.ysucloud.exapis.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:33
 */
@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    MessageService messageService;

    @PostMapping("/save")
    public ResultVO save(@RequestBody JSONObject jsonObject){
        String message = jsonObject.getString("message");

        if(StringUtils.isEmpty(message)){
            throw new NullParameterException();
        }

        messageService.save(message);

        return ResultVOUtil.success(null);
    }

    @PostMapping("/get")
    public ResultVO get(){

        return ResultVOUtil.success(messageService.findAll());
    }
}
