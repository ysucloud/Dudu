package com.ysucloud.exapis.controller;

import com.alibaba.fastjson.JSONObject;
import com.ysucloud.exapis.Utils.ResultVOUtil;
import com.ysucloud.exapis.VO.ResultVO;
import com.ysucloud.exapis.exception.NullParameterException;
import com.ysucloud.exapis.form.CommentForm;
import com.ysucloud.exapis.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: exapis
 * Created by 刘翰文 on 2019/7/2 12:48
 */

/** 回复 */
@RestController
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    CommentService commentService;

    @PostMapping("/save")
    public ResultVO save(@RequestBody @Validated CommentForm commentForm,
                         BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            throw new NullParameterException();
        }

        commentService.save(commentForm);

        return ResultVOUtil.success(null);
    }

    @PostMapping("/list")
    public ResultVO get(@RequestBody JSONObject jsonObject){
        Integer productId = jsonObject.getInteger("id");

        if(productId == null){
            throw new NullParameterException();
        }

        return ResultVOUtil.success(commentService.findAll(productId));
    }
}
