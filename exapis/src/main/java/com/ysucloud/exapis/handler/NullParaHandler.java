package com.ysucloud.exapis.handler;

import com.ysucloud.exapis.VO.ResultVO;
import com.ysucloud.exapis.exception.NullParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;


/**
 * 拦截空参数异常
 *
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 22:37
 */
@RestController
@ControllerAdvice
public class NullParaHandler {
    /** 空参数 */
    @ExceptionHandler(value = NullParameterException.class)
    public ResultVO NullSelectHandler(Exception e){
        ResultVO result = new ResultVO();
        result.setCode(10001);
        result.setMsg("参数为空！");

        return result;
    }

}
