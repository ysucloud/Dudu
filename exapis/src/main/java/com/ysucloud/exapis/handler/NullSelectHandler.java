package com.ysucloud.exapis.handler;

import com.ysucloud.exapis.VO.ResultVO;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 19:09
 */
@RestController
@ControllerAdvice
public class NullSelectHandler {

    /** 空查询 */
    @ExceptionHandler(value = NoSuchElementException.class)
    public ResultVO NullSelectHandler(Exception e){
        ResultVO result = new ResultVO();
        result.setCode(10006);
        result.setMsg("数据库不存在该数据！");

        return result;
    }

    /** 空主键删除 */
    @ExceptionHandler(value = EmptyResultDataAccessException.class)
    public ResultVO NullDeleteHandler(Exception e){
        ResultVO result = new ResultVO();
        result.setCode(10006);
        result.setMsg("数据库不存在该数据！");

        return result;
    }

}
