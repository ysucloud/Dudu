package com.ysucloud.manager.VO;

import lombok.Data;


/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/29 18:21
 */
@Data
public class CountVO {
    private Integer allCount;
    private Integer weekCount;
    private Integer mouthCount;
}
