package com.ysucloud.manager.VO;

import com.ysucloud.manager.enums.ResultCode;
import lombok.Data;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 18:34
 */
@Data
public class ResultObjectVO<T> {
    private Integer Code = ResultCode.SUCCESS.getCode();
    private String Msg = ResultCode.SUCCESS.getMsg();

    T data;

    public ResultObjectVO (T object){
        this.data = object;
    }

    public ResultObjectVO (){

    }
}
