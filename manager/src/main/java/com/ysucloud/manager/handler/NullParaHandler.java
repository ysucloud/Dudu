package com.ysucloud.manager.handler;

import com.ysucloud.manager.VO.ResultObjectVO;
import com.ysucloud.manager.exception.NullParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;


/**
 * 拦截空参数异常
 *
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 22:37
 */
@RestController
@ControllerAdvice
public class NullParaHandler {
    /** 空参数 */
    @ExceptionHandler(value = NullParameterException.class)
    public ResultObjectVO NullSelectHandler(Exception e){
        ResultObjectVO result = new ResultObjectVO();
        result.setCode(10001);
        result.setMsg("参数为空！");

        return result;
    }

}
