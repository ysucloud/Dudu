package com.ysucloud.manager.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 9:21
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Data
public class ProductCategory {

    /** 类目id */
    @Id
    @GeneratedValue
    private Integer categoryId;

    /** 类目名字 */
    private String categoryName;

    /** 自动时间戳*/
    @Column(insertable = false,updatable = false)
    @Generated(GenerationTime.INSERT)
    private Date createTime;
    @Column(insertable = false,updatable = false)
    @Generated(GenerationTime.ALWAYS)
    private Date updateTime;

}
