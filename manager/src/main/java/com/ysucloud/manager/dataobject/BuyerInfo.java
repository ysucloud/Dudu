package com.ysucloud.manager.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.util.Date;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 9:18
 */
@Entity
@Data
@Table(name = "buyer_info")
@DynamicInsert
@DynamicUpdate
public class BuyerInfo {
    /** 用户主键（id） */
    @Id
    @GeneratedValue
    private Integer id;

    /** 用户名（昵称） */
    private String username;
    /** 微信openid */
    private String openid;

    /** 自动时间戳 */
    @Column(insertable = false,updatable = false)
    @Generated(GenerationTime.INSERT)
    private Date createTime;
    @Column(insertable = false,updatable = false)
    @Generated(GenerationTime.ALWAYS)
    private Date updateTime;

}
