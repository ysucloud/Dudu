package com.ysucloud.manager.converter;

import com.ysucloud.manager.dataobject.ProductInfo;
import com.ysucloud.manager.form.ProductForm;
import com.ysucloud.manager.form.ProductWidForm;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 23:52
 */
public class ProductWidFormToProductInfo {
    public static ProductInfo convert(ProductWidForm product){
        ProductInfo productInfo = new ProductInfo();

        productInfo.setProductId(product.getId());

        /** 名字 */
        productInfo.setProductName(product.getName());

        /** 单价 */
        productInfo.setProductPrice(product.getPrice());

        /** 库存 */
        productInfo.setProductStock(product.getStock());

        /** 描述 */
        productInfo.setProductDescription(product.getDescription());

        /** 小图 */
        productInfo.setProductIcon(product.getIcon());

        /** 状态, 1（true）正常0（false）下架2已删除 */
        productInfo.setProductStatus(0);

        /** 类目编号 */
        productInfo.setCategoryId(product.getCategory_id());

        return productInfo;
    }
}
