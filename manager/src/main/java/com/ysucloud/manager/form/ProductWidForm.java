package com.ysucloud.manager.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 23:51
 */
@Data
public class ProductWidForm {

    @NotNull
    private Integer id;
    @NotEmpty
    private String name;
    @NotNull
    private BigDecimal price;
    @NotNull
    private Integer stock;
    @NotEmpty
    private String description;
    @NotEmpty
    private String icon;
    @NotNull
    private Integer category_id;
}
