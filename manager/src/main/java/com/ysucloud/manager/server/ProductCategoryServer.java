package com.ysucloud.manager.server;

import com.ysucloud.manager.dataobject.ProductCategory;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 12:21
 */

public interface ProductCategoryServer {

    List<ProductCategory> getAllProductCategory();

    Boolean saveProductCategory(ProductCategory productCategory);

    Boolean changeProductCategory(ProductCategory productCategory);

    Boolean deleteProductCategory(Integer id);

}
