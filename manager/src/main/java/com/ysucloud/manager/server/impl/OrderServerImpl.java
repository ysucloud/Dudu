package com.ysucloud.manager.server.impl;

import com.ysucloud.manager.DTO.CountDTO;
import com.ysucloud.manager.DTO.OrderDTO;
import com.ysucloud.manager.Utils.TimeUtil;
import com.ysucloud.manager.dataobject.OrderDetail;
import com.ysucloud.manager.dataobject.OrderMaster;
import com.ysucloud.manager.repository.OrderDetailRepository;
import com.ysucloud.manager.repository.OrderMasterRepository;
import com.ysucloud.manager.server.OrderServer;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:43
 */
@Service
public class OrderServerImpl implements OrderServer {
    @Autowired
    OrderMasterRepository orderMasterRepository;

    @Autowired
    OrderDetailRepository orderDetailRepository;

    @Override
    public List<OrderDTO> getAll() {
        List<OrderMaster> orderList = orderMasterRepository.findAll();

        List<OrderDTO> result = new ArrayList<OrderDTO>();

        for(OrderMaster order:orderList){
            OrderDTO orderDTO = new OrderDTO();

            BeanUtils.copyProperties(order,orderDTO);

            List<OrderDetail> orderDetailS =
                    orderDetailRepository.findByOrderId(order.getOrderId());
            orderDTO.setDetails(orderDetailS);

            result.add(orderDTO);
        }

        return result;
    }

    @Override
    public OrderDTO getOne(Integer orderId) {
        OrderMaster orderMaster = orderMasterRepository.findById(orderId).get();

        List<OrderDetail> orderDetails = orderDetailRepository.findByOrderId(orderId);

        OrderDTO orderDTO = new OrderDTO();

        BeanUtils.copyProperties(orderMaster,orderDTO);

        orderDTO.setDetails(orderDetails);

        return orderDTO;
    }

    @Override
    public Boolean finishOrder(Integer id) {
        OrderMaster orderMaster =
                orderMasterRepository.findById(id).get();



        orderMaster.setOrderStatus(1);
        orderMasterRepository.save(orderMaster);

        return true;
    }

    @Override
    public CountDTO countWeek() {
        List<Date> week = TimeUtil.weekDate();

        CountDTO countDTO = new CountDTO();

        countDTO.setOne(orderMasterRepository.countByDate(week.get(1),week.get(0)));
        countDTO.setTwo(orderMasterRepository.countByDate(week.get(2),week.get(1)));
        countDTO.setThree(orderMasterRepository.countByDate(week.get(3),week.get(2)));
        countDTO.setFour(orderMasterRepository.countByDate(week.get(4),week.get(3)));
        countDTO.setFive(orderMasterRepository.countByDate(week.get(5),week.get(4)));
        countDTO.setSix(orderMasterRepository.countByDate(week.get(6),week.get(5)));
        countDTO.setSeven(orderMasterRepository.countByDate(week.get(7),week.get(6)));

        return countDTO;
    }
}
