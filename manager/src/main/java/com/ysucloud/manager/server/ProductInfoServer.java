package com.ysucloud.manager.server;

import com.ysucloud.manager.dataobject.ProductInfo;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 13:53
 */
public interface ProductInfoServer {
    List<ProductInfo> getAllProduct();

    Boolean changeProduct(ProductInfo productInfo);

    Boolean saveProduct(ProductInfo productInfo);

    Boolean deleteProduct(Integer id);
}
