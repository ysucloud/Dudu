package com.ysucloud.manager.server.impl;

import com.ysucloud.manager.DTO.CountDTO;
import com.ysucloud.manager.Utils.TimeUtil;
import com.ysucloud.manager.dataobject.BuyerInfo;
import com.ysucloud.manager.repository.BuyerInfoRepository;
import com.ysucloud.manager.server.BuyerServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:34
 */
@Service
public class BuyerServerImpl implements BuyerServer {
    @Autowired
    BuyerInfoRepository buyerInfoRepository;

    @Override
    public List<BuyerInfo> getAll() {
        List<BuyerInfo> buyerList = buyerInfoRepository.findAll();
        return buyerList;
    }

    @Override
    public Integer getAllCount(){
        return buyerInfoRepository.countAllBy();
    }

    @Override
    public CountDTO countWeek() {
        List<Date> week = TimeUtil.weekDate();

        CountDTO countDTO = new CountDTO();

        countDTO.setOne(buyerInfoRepository.countByDate(week.get(1),week.get(0)));
        countDTO.setTwo(buyerInfoRepository.countByDate(week.get(2),week.get(1)));
        countDTO.setThree(buyerInfoRepository.countByDate(week.get(3),week.get(2)));
        countDTO.setFour(buyerInfoRepository.countByDate(week.get(4),week.get(3)));
        countDTO.setFive(buyerInfoRepository.countByDate(week.get(5),week.get(4)));
        countDTO.setSix(buyerInfoRepository.countByDate(week.get(6),week.get(5)));
        countDTO.setSeven(buyerInfoRepository.countByDate(week.get(7),week.get(6)));

        return countDTO;
    }
}
