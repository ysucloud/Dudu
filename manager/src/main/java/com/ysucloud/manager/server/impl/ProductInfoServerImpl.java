package com.ysucloud.manager.server.impl;

import com.ysucloud.manager.dataobject.OrderMaster;
import com.ysucloud.manager.dataobject.ProductInfo;
import com.ysucloud.manager.repository.OrderDetailRepository;
import com.ysucloud.manager.repository.ProductCategoryRepository;
import com.ysucloud.manager.repository.ProductInfoRepository;
import com.ysucloud.manager.server.ProductInfoServer;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:03
 */
@Service
public class ProductInfoServerImpl implements ProductInfoServer {
    @Autowired
    ProductCategoryRepository productCategoryRepository;

    @Autowired
    ProductInfoRepository productInfoRepository;

    @Autowired
    OrderDetailRepository orderDetailRepository;

    @Override
    public List<ProductInfo> getAllProduct() {
        List<ProductInfo> list = productInfoRepository.all();

        return list;
    }

    /** 保存，校验品类是否存在 */
    @Override
    public Boolean saveProduct(ProductInfo productInfo) {
        Integer category_id = productInfo.getCategoryId();

        productCategoryRepository.findById(category_id).get();

        productInfoRepository.save(productInfo);

        return true;
    }

    /**
     *  软删除逻辑实现
     */
    @Override
    public Boolean deleteProduct(Integer id) {
        ProductInfo product = productInfoRepository.findById(id).get();

        /** 删除 */
        product.setProductStatus(2);
        productInfoRepository.save(product);

        return true;
    }

    /**
     * 修改，检查商品ID是否存在
     * create_time注意不要修改
     */
    @Override
    public Boolean changeProduct(ProductInfo productInfo) {
        /** 保证更新前有该数据 */
        ProductInfo productOld = productInfoRepository.findById(productInfo.getProductId()).get();

        //BeanUtils.copyProperties(productOld,productInfo);
        /** 获取旧的create_time防止修改 */
        productInfo.setCreateTime(productOld.getCreateTime());

        productInfoRepository.save(productInfo);

        return true;
    }
}
