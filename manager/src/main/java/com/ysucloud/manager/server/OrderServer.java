package com.ysucloud.manager.server;

import com.ysucloud.manager.DTO.CountDTO;
import com.ysucloud.manager.DTO.OrderDTO;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:37
 */
public interface OrderServer{
    List<OrderDTO> getAll();

    OrderDTO getOne(Integer orderId);

    Boolean finishOrder(Integer id);

    CountDTO countWeek();
}
