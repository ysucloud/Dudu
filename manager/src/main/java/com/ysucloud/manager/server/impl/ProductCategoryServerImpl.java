package com.ysucloud.manager.server.impl;

import com.ysucloud.manager.dataobject.ProductCategory;
import com.ysucloud.manager.repository.ProductCategoryRepository;
import com.ysucloud.manager.server.ProductCategoryServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 12:25
 */
@Service
public class ProductCategoryServerImpl implements ProductCategoryServer {

    @Autowired
    ProductCategoryRepository productCategoryRepository;

    @Override
    /** 查询 */
    public List<ProductCategory> getAllProductCategory() {
        List<ProductCategory> productCategoryList =
                productCategoryRepository.findAll();
        return productCategoryList;
    }

    /** 新增 */
    @Override
    public Boolean saveProductCategory(ProductCategory productCategory) {

        productCategoryRepository.save(productCategory);

        return true;
    }

    /** 删除（直接删除） */
    @Override
    public Boolean deleteProductCategory(Integer id) {
            productCategoryRepository.deleteById(id);

        return true;
    }

    @Override
    public Boolean changeProductCategory(ProductCategory productCategory) {
        ProductCategory productCategory1Old =
                productCategoryRepository.findById(productCategory.getCategoryId()).get();

        //productCategory.setCreateTime(productCategory1Old.getCreateTime());

        productCategoryRepository.save(productCategory);

        return true;
    }
}
