package com.ysucloud.manager.server;

import com.ysucloud.manager.DTO.CountDTO;
import com.ysucloud.manager.dataobject.BuyerInfo;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:32
 */
public interface BuyerServer {
    /** 返回全体用户列表 */
    List<BuyerInfo> getAll();

    /** 返回用户的数量信息（全体） */
    Integer getAllCount();

    CountDTO countWeek();
}
