package com.ysucloud.manager.repository;

import com.ysucloud.manager.dataobject.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 12:26
 */
public interface ProductCategoryRepository extends JpaRepository<ProductCategory,Integer> {


}
