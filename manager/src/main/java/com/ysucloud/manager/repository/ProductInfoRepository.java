package com.ysucloud.manager.repository;

import com.ysucloud.manager.dataobject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:04
 */
public interface ProductInfoRepository extends JpaRepository<ProductInfo,Integer> {

    @Query(value = "select * from product_info where product_status in (1,0)",
            nativeQuery = true)
    List<ProductInfo> all();

}
