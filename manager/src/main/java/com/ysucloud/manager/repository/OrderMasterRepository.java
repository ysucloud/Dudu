package com.ysucloud.manager.repository;

import com.ysucloud.manager.dataobject.OrderMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:05
 */
public interface OrderMasterRepository extends JpaRepository<OrderMaster,Integer> {

    Integer countAllBy();

    /** 查询订单数量 */
    @Query(value = "select count(*) from order_master where create_time >= :min and create_time <= :max and order_status in (1,0)",
            nativeQuery = true)
    Integer countByDate(@Param("min") Date min, @Param("max") Date max);
}
