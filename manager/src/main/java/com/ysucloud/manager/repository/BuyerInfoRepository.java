package com.ysucloud.manager.repository;

import com.ysucloud.manager.dataobject.BuyerInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:05
 */
public interface BuyerInfoRepository extends JpaRepository<BuyerInfo,Integer> {

    Integer countAllBy();

    @Query(value = "select count(*) from buyer_info where create_time >= :min and create_time <= :max",
            nativeQuery = true)
    Integer countByDate(@Param("min") Date min, @Param("max") Date max);
}
