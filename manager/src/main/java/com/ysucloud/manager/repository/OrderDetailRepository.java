package com.ysucloud.manager.repository;

import com.ysucloud.manager.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:06
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail,Integer> {
    List<OrderDetail> findByProductId(Integer productId);

    List<OrderDetail> findByOrderId(Integer orderId);

}
