package com.ysucloud.manager.controller;

import com.alibaba.fastjson.JSONObject;
import com.ysucloud.manager.Utils.TimeUtil;
import com.ysucloud.manager.VO.CountVO;
import com.ysucloud.manager.VO.ResultListVO;
import com.ysucloud.manager.VO.ResultObjectVO;
import com.ysucloud.manager.dataobject.BuyerInfo;
import com.ysucloud.manager.exception.NullParameterException;
import com.ysucloud.manager.repository.BuyerInfoRepository;
import com.ysucloud.manager.server.BuyerServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 17:15
 */
@RestController
@RequestMapping("/user")
public class BuyerController {
    @Autowired
    BuyerServer buyerServer;

    @Autowired
    BuyerInfoRepository buyerInfoRepository;

    /** 获取买家详情
     * 一个
     */
    @PostMapping("/get")
    public ResultObjectVO getOne(@RequestBody JSONObject jsonObject){
        if(jsonObject.getInteger("id") == null){
            throw new NullParameterException();
        }

        BuyerInfo buyerInfo = buyerInfoRepository.findById(jsonObject.getInteger("id")).get();

        return new ResultObjectVO(buyerInfo);
    }


    /** 获取买家用户列表 */
    @PostMapping("/list")
    public ResultListVO getAll(){
        List<BuyerInfo> list = buyerServer.getAll();

        ResultListVO result = new ResultListVO();
        result.setData(list);

        return result;
    }

    /** 获取用户注册量 */
    @PostMapping("/count")
    public ResultObjectVO getCount(){
        CountVO countVO = new CountVO();
        countVO.setAllCount(buyerServer.getAllCount());
        countVO.setWeekCount(buyerInfoRepository.countByDate(TimeUtil.addDate(- 7),new Date()));
        countVO.setMouthCount(buyerInfoRepository.countByDate(TimeUtil.addDate(- 30),new Date()));

        ResultObjectVO result = new ResultObjectVO();
        result.setData(countVO);

        return result;
    }

    /** 一周内用户数量（每日） */
    @PostMapping("/week")
    public ResultObjectVO weekCount(){
        return new ResultObjectVO(buyerServer.countWeek());
    }

}
