package com.ysucloud.manager.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/29 18:17
 */
public class TimeUtil {
    public static Date addDate(Integer date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE,date);
        return calendar.getTime();
    }

    public static List<Date> weekDate(){
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH) +2, 0, 0, 0);
        List<Date> week = new ArrayList<Date>();

        for(int i = 0;i<8;i++){
            cal.add(Calendar.DATE,- 1);
            week.add(cal.getTime());
        }

        return week;
    }
}
