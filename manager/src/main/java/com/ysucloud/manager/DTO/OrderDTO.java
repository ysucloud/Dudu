package com.ysucloud.manager.DTO;

import com.ysucloud.manager.dataobject.OrderDetail;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 14:40
 */
@Data
public class OrderDTO {
    /** 订单id */
    private Integer orderId;

    /** 买家名字 */
    private String buyerName;

    /** 买家手机号 */
    private String buyerPhone;

    /** 买家地址 */
    private String buyerAddress;

    /** 买家用户ID */
    private Integer buyerId;

    /** 订单总金额 */
    private BigDecimal orderAmount;

    /** 订单状态, 为0新下单，1已完成，2取消 */
    private Integer orderStatus;

    /** 支付状态, 默认为0未支付，1支付 */
    private Integer payStatus;

    /** 自动时间戳 */
    private Date createTime;
    private Date updateTime;

    /** 订单详情项 */
    private List<OrderDetail> details;

}
