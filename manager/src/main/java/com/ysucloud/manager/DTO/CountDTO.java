package com.ysucloud.manager.DTO;

import lombok.Data;

/**
 * Description: api
 * Created by 刘翰文 on 2019/6/30 10:32
 */
@Data
public class CountDTO {
    private Integer One;
    private Integer Two;
    private Integer Three;
    private Integer Four;
    private Integer Five;
    private Integer Six;
    private Integer Seven;
}
