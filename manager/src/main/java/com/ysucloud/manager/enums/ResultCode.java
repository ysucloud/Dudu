package com.ysucloud.manager.enums;

import lombok.Data;
import lombok.Getter;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 13:33
 */
@Getter
public enum ResultCode {
    SUCCESS(200,"OK"),
    ;

    private Integer code;
    private String msg;

    ResultCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
