Component({
  options: {
    multipleSlots: true
  },
  properties: {
    //返回 默认和标题相同颜色
    back: String,//true/false
    backColor: String,//#fff
    //标题 标题颜色
    title: String,
    fontColor: String,
    fontSize: String,
    titleStyle: String,
    //背景颜色
    backgroundColor: String,
    //底层样式
    belowColor: String,
    belowHeight: String,
    belowSrc: String,
    //占位
    placeHolder: String,

  },
  data: {
    navigationBarHeight: 0
  },
  attached: function () {
    let that = this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          navigationBarHeight: res.statusBarHeight + 44
        })
      },
    })
  },
  methods: {
    onBack: (event) => {
      wx.navigateBack({
        delta: 1
      })
    }
  }
})