let statusBarHeight = 0
let navigationBarHeight = 0
let getSystemInfo = function() {
  const res = wx.getSystemInfoSync()
  statusBarHeight = res.statusBarHeight
  navigationBarHeight = res.statusBarHeight + 44
}
let getStatusBarHeight = function() {
  if (statusBarHeight == 0)
    getSystemInfo()
  return statusBarHeight
}
let getNavigationBarHeight = function() {
  if (navigationBarHeight == 0)
    getSystemInfo()
  return navigationBarHeight
}
module.exports = {
  getStatusBarHeight: getStatusBarHeight,
  getNavigationBarHeight: getNavigationBarHeight,
};