let debug = function(msg) {
  if (true)
    console.log(msg)
}

let setToken = function(token) {
  wx.setStorageSync('token', token);
  debug('Token设置:' + token + ' 成功');
}

let getToken = function() {
  let token = wx.getStorageSync('token');
  debug('Token获取:' + token);
  return token;
}

let removeToken = function() {
  wx.removeStorageSync('token');
  debug('Token移除:成功');
}

let request = function ({url, data, header={}, 
  method = 'POST'
  , success, fail }) {
  let _url = url;
  let _data = data;
  header['token'] = getToken();
  let _header = header;
  let _method = method;
  let _success = success;
  let _fail = fail;

  debug('HTTP开始请求: ' + _method +' '+ _url)
  wx.request({
    url: _url,
    data: _data,
    header: _header,
    method:_method,
    success: function(result) {
      debug('HTTP成功回调:')
      debug(result)
      _success(result);
    },
    fail: function(result) {
      debug('HTTP失败回调:')
      debug(result)
      _fail(result);
    }
  })
};


module.exports = {
  request: request,
  setToken: setToken,
  getToken: getToken,
  removeToken: removeToken
}