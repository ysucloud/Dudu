//app.js
App({



  post: function (url, data) {
    var promise = new Promise((resolve, reject) => {
      //init
      var that = this;
      var postData = data;
      wx.request({
        url: url,
        data: postData,
        method: 'POST',
        success: function (res) { //服务器返回数据
          resolve(res);
        },
        error: function (e) {
          reject('网络出错');
        }
      })
    });
    return promise;
  },
  login(e) {
    var that = this
    var url = "https://ks.erhuotuzi.cn/users/buyer/login"
    wx.login({
      success: function (res) {
        if (res.code) {
          var d = {
            "code": res.code
          }
          wx.request({
            url: url,
            data: d,
            method: "POST",
            success(e) {
              wx.setStorageSync("uid", e.data.data.id)
              wx.setStorageSync("token", e.data.data.token)
            }
          })
        }
      }
    })
  },
  onLaunch: function () {
    this.login()
  },
  globalData: {
    userInfo: null,
    url:"https://ks.erhuotuzi.cn/",
    php:"https://ksphp.erhuotuzi.cn/"
  }
})