// pages/order/order.js
const http = require('../../utils/http.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    payWay:"在线支付"
  },

  /**
   * 生命周期函数--监听页面加载
   */

  submitOrder(callback){
    var items=[]
    var address = this.data.address
    
    for(let i=0;i<this.data.product.length;i++){
      var product = {}
      product.productId = this.data.product[i].id
      product.productQuantity = this.data.product[i].counts
      items.push(product)
    }
    
    // for(let i = 0;i<this.data.product.length;i++){
    //   product.id = this.data.product[i].id
    //   product.count = this.data.product[i].count 
    //   items.push(product)
    // }
    var d={
      "name": this.data.address.userName,
      "phone": this.data.address.telNumber,
      "buyerId":wx.getStorageSync("uid"),
      "items":items,
      "address":address.provinceName+address.cityName+address.countyName+address.detailInfo
    }
    http.setToken(wx.getStorageSync("token"))
    var url = getApp().globalData.url
    http.request({
      url: url + "apis/order/create",
      method: "POST",
      data:d,
      success(e) {
        callback(e)
      }
    })
  },

 

  onAddressTap(e){
    var that = this
    wx.chooseAddress({
      success(e){
        //console.log(e)
        that.setData({
          address:e
        })
      }
    })
  },
  onCouponTap(e){
    wx.navigateTo({
      url: 'coupon/coupon',
    })
  },

  onPayWayTap(e){
    var that = this
    wx.showActionSheet({
      itemList: ["在线支付","朋友代付","货到付款"],
      success(e){
        if(e.tapIndex==0){
          that.setData({
            payWay:"在线支付"
          })
        }else if(e.tapIndex==1){
          that.setData({
            payWay: "朋友代付"
          })
        } else if (e.tapIndex == 2) {
          that.setData({
            payWay: "货到付款"
          })
        }
      }
    })
  },
  submit(e){
    if(!this.data.address){
      wx.showToast({
        title: '请先填写地址',
        duration:1300,
        icon:'none'
      })
    }
    else{
      this.submitOrder((res)=>{
        console.log(res)
        wx.navigateTo({
          url: '../pay/pay?orderId=' + res.data.data.orderId,
        })
      })
      // wx.navigateTo({
      //   url: '../pay/pay',
      // })
    }
  },
  onLoad: function (e) {
    var product = JSON.parse(e.product)
    var totalPrice=0

    for(let i = 0;i<product.length;i++){
      var tmp=0
      tmp=product[i].price*product[i].counts
      totalPrice+=tmp
    } this.setData({
      product: product,
      totalPrice:totalPrice
    })
    console.log(product)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})