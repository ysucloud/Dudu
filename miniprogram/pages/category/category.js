// pages/category/category.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentMenuIndex: 0,
    transClassArr: ['tanslate0', 'tanslate1', 'tanslate2', 'tanslate3', 'tanslate4', 'tanslate5'],
    //   categoryTypeArr: [{
    //     name: "食品生鲜",
    //     id: 1
    //   }, {
    //     name: "滋补保健",
    //     id: 2
    //   }, {
    //     name: "地方特产",
    //     id: 3
    //   }, {
    //     name: "畜牧产品",
    //     id: 4
    //   }, {
    //     name: "种植产品",
    //     id: 5
    //   }, {
    //     name: "渔业产品",
    //     id: 6
    //   }, ]
  },
  getData(e) {
    var that = this
    var url = getApp().globalData.url
    wx.request({
      url: url + "/apis/product/list",
      method: "POST",
      success(e) {
        that.setData({
          categoryTypeArr: e.data.data,
          categoryProducts: e.data.data[0].foods
        })
      }
    })
  },

  onProductsItemTap(e) {
    var id =e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../home/detail/detail?id='+id,
    })
  },


  changeCategory: function(event) {
    var index = event.currentTarget.dataset.index
    this.setData({
      categoryProducts: this.data.categoryTypeArr[index].foods
    })

    this.setData({
      currentMenuIndex: index
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getData()
    if (options.index) {
      this.setData({
        currentMenuIndex: options.index
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})