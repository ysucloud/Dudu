Page({
  data: {

  },

  onLogoutTap(e) {
    wx.navigateTo({
      url: 'message/message',
    })
  },
  onAllOrderTap(e) {
    wx.navigateTo({
      url: 'orders/orders?a=' + 0,
    })
  },
  onOneTap(e) {
    wx.navigateTo({
      url: 'orders/orders?a=' + 1,
    })
  },
  onTwoTap(e) {
    wx.navigateTo({
      url: 'orders/orders?a=' + 2,
    })
  },
  onThreeTap(e) {
    wx.navigateTo({
      url: 'orders/orders?a=' + 3,
    })
  },
  onFourTap(e) {
    wx.navigateTo({
      url: 'orders/orders?a=' + 4,
    })
  },
  onFiveTap(e) {
    wx.navigateTo({
      url: 'orders/orders?a=' + 5,
    })
  },
  onAddressTap(e) {
    wx.chooseAddress()
  },
  onContactTap(e) {
    wx.makePhoneCall({
      phoneNumber: '12345678910',
    })
  },
  onLoad: function(options) {

  },
  onReady: function() {

  },
  onShow: function() {

  },
  onHide: function() {

  },
  onUnload: function() {

  },
  onPullDownRefresh: function() {

  }
})