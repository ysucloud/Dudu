// pages/my/orders/orders.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    currentTabsIndex: 0,
    tab: ["全部", '待付款', '待收货', '待评价', '已完成', '退款中']
  },

  onComment(e){
    var id = e.currentTarget.dataset.oid
    wx.navigateTo({
      url: '../comment/comment?id='+id,
    })
  },
  onSure(e){
    var that = this
    var url = getApp().globalData.php
    wx.request({
      url: url + "order/confirm",
      method: "POST",
      data: {
        "order_id": e.currentTarget.dataset.oid,
      },
      success(e) {
        if (e.data.error_code == 0) {
          wx.showToast({
            title: '确认成功！',
            duration: 1300
          })
          that.getAllOrders()
        }
      }
    }) 
  },
  onCancel(e){
    var that = this
    var url = getApp().globalData.url
    wx.request({
      url: url + "apis/order/cancel",
      method: "POST",
      data: {
        "orderId": e.currentTarget.dataset.oid,
        "buyerId":wx.getStorageSync("uid")
      },
      success(e) {
        if (e.data.code == 0) {
          wx.showToast({
            title: '取消成功！',
            duration: 1300
          })
          that.getAllOrders()
        }
      }
    })
  },
  gopay(e){
    var tmp = this.data.list[e.currentTarget.dataset.index].order_detail

    var p=[]
    for (let i = 0; i < tmp.length;i++){
      var a={}
      a.id = tmp[i].product_id
      a.name = tmp[i].product_name
      a.price = tmp[i].product_price
      a.url = tmp[i].product_icon
      a.counts = tmp[i].product_quantity
      p.push(a)
    }
    console.log(p)
     p = JSON.stringify(p)
    wx.navigateTo({
      url: '../../order/order?product='+p,
    })
  },
  deleteOrder(e) {
    var that = this
    var url = getApp().globalData.php
    wx.request({
      url: url + "order/delete",
      method: "POST",
      data: {
        "order_id": e.currentTarget.dataset.id
      },
      success(e) {
        if (e.data.error_code == 0) {
          wx.showToast({
            title: '删除成功！',
            duration: 1300
          })
          that.getAllOrders()
        }
      }
    })
  },
  onTabsItemTap: function(e) {
    var index = e.currentTarget.dataset.index
    var list = this.data.list
    if (index == 1) {


    }

    this.setData({
      currentTabsIndex: index
    });
  },
  getAllOrders(e) {

    var that = this
    var url = getApp().globalData.php
    wx.request({
      url: url + "order/list",
      method: "POST",
      data: {
        "buyerId": wx.getStorageSync("uid")
      },
      success(e) {
        var list = e.data
        for (let i = 0; i < e.data.length; i++) {
          if (list[i].order_status == 0 && list[i].pay_status == 0) {
            list[i].flag = 1
          } else if (list[i].order_status == 0 && list[i].pay_status == 1) {
            list[i].flag = 2
          } else if (list[i].order_status == 3 && list[i].pay_status == 1) {
            list[i].flag = 3
          } else if (list[i].order_status == 1) {
            list[i].flag = 4
          } else if (list[i].order_status == 2 && list[i].pay_status == 1) {
            list[i].flag = 5
          }
          list[i].total = 0
          for (let j = 0; j < list[i].order_detail.length;j++){
            list[i].total += list[i].order_detail[j].product_quantity
          }
        }
        that.setData({
          list: list
        })
      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    this.getAllOrders()
    var a= e.a
    this.setData({
      currentTabsIndex:a
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})