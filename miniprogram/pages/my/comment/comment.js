// pages/my/comment/comment.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:''
  },

  onInput(e){
    this.setData({
      info:e.detail.value
    })
  },
  onConfirm(e){
    var that = this
    var url = getApp().globalData.php
    wx.request({
      url: url + "order/comment",
      method: "POST",
      data: {
        "order_id": this.data.id,
        "info":this.data.info
      },
      success(e) {
        if (e.data.error_code == 0) {
          wx.showToast({
            title: '评价成功！',
            duration: 1300
          })
          setTimeout(function(){
            wx.switchTab({
              url: '../../my/my',
            })
          },1250)
        }
      }
    }) 
  },
  onLoad: function (e) {
    this.setData({
      id:e.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})