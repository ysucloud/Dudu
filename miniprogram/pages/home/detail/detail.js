// pages/medicine/detail/detail.js
import {
  Cart
} from '../../cart/cart-model.js';

var cart = new Cart()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: 0,
    buynum: 0,
    productCounts: 1,
    cartTotalCounts: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onAllComments(e) {
    var a = this.data.comment
    a = JSON.stringify(a)
    wx.navigateTo({
      url: '../all-comments/all-comments?all=' + a,
    })
  },
  loadComments(e) {
    var that = this
    var url = getApp().globalData.php
    wx.request({
      url: url + "comment/list",
      method: "POST",
      data: {
        id: this.data.id
      },
      success(e) {
        that.setData({
          comment: e.data
        })
      }
    })
  },

  loadDetail(callback) {
    var that = this
    var url = getApp().globalData.url
    wx.request({
      url: url + "apis/product/get",
      method: "POST",
      data: {
        id: this.data.id
      },
      success(e) {
        callback(e.data.data)
        that.setData({
          info: e.data.data
        })
      }
    })
  },
  onAddingToCartTap: function() {
    var tempObj = {},
      keys = ['id', 'name', 'url', 'price'];
    for (var key in this.data.product) {
      if (keys.indexOf(key) >= 0) {
        tempObj[key] = this.data.product[key];
      }
    }
    cart.add(tempObj, this.data.productCounts);

    var counts = this.data.cartTotalCounts + this.data.productCounts;
    this.setData({
      cartTotalCounts: counts
    });
    wx.showToast({
      title: '添加成功！',
      mask: true,
      duration: 1000
    })
  },





  onSureTap(e) {
    if (this.data.buynum == 0) {
      wx.showToast({
        title: '请先选择数量哦~',
        duration: 1300,
        icon: "none"
      })
    } else {
      var tmp = []
      var p = this.data.product
      p.counts = this.data.buynum
      tmp.push(p)
      tmp = JSON.stringify(tmp)
      wx.navigateTo({
        url: '../../order/order?product=' + tmp,
      })
    }

  },
  onCartTap(e) {
    wx.switchTab({
      url: '../../cart/cart',
    })
  },

  onSub(e) {
    var num
    if (this.data.buynum <= 1) {
      num = 0
    } else {
      num = this.data.buynum - 1
    }
    this.setData({
      buynum: num
    })
  },
  onAdd(e) {
    this.data.buynum++
      this.setData({
        buynum: this.data.buynum++
      })
  },

  onHiddenToastTap(e) {
    this.setData({
      show: false
    })
  },
  onBuyTap(e) {
    this.setData({
      show: true
    })
  },



  onLoad: function(e) {
    this.setData({
      id: e.id
    })
    this.loadDetail((res) => {
      var product = {
        id: res.productId,
        name: res.productName,
        price: res.productPrice,
        url: res.productIcon
      }
      this.setData({
        product: product,
        cartTotalCounts: cart.getCartTotalCounts().counts1
      })
    })
    this.loadComments(e.id)

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})