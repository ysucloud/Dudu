const ui = require('../../utils/ui.js')
Page({

  data: {

  },
  onGotUserInfo: function (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.userInfo)
    console.log(e.detail.rawData)
  },
  onRushTap(e){
    wx.navigateTo({
      url: '../new/new',
    })
  },
  onRewardTap(e){
    wx.navigateTo({
      url: '../reward/reward',
    })
  },

  onSecondTap(e) {
    wx.navigateTo({
      url: '../second/second',
    })
  },
  onHotTap(e) {
    var index = e.currentTarget.dataset.index
    wx.switchTab({
      url: '../category/category',
    })
  },
  onCalendarTap(e) {
    wx.navigateTo({
      url: '/pages/calendar/calendar',
    })
  },
  getCommend(e) {
    var that = this
    var url = getApp().globalData.php
    wx.request({
      url: url + "tj/get",
      method: "POST",
      success(e) {
        for(let i=0;i<e.data.length;i++){
          e.data[i].h = (e.data[i].product_price * 1.2).toFixed(2)
        }
        that.setData({
          commend: e.data
        })
      }
    })
  },
  onFoodTap(e) {
    var id  = e.currentTarget.dataset.id
    wx.navigateTo({
      url: 'detail/detail?id='+id,
    })
  },

  onLoad: function(options) {
    
    this.getCommend()
    this.setData({
      statusBarHeight: ui.getStatusBarHeight(),
      navigationBarHeight: ui.getNavigationBarHeight()
    })
  },

  onReady: function() {

  },


  onShow: function() {

  },

  onPageScroll: function(e) {
    // console.log(e)
    let percent = 0;
    if (e.scrollTop <= 40) {
      percent = 0;
    } else if (e.scrollTop >= 140) {
      percent = 1;
    } else {
      percent = (e.scrollTop - 40) * 0.01;
    }
    this.setData({
      percent: percent
    })
  }
})