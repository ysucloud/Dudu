let monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
let month = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
let time = new Date()
let thisY = time.getFullYear() //正确年
let thisM = time.getMonth() //少一个月
let thisD = time.getDate() //正确日期

let cal = function() {
  console.log(thisY)
  console.log(thisM)
  monthDays[1] = (thisY % 400 == 0 || (thisY % 4 == 0 && thisY % 100 == 0)) ? 29 : 28 //今年二月有几天
  let realM = thisM + 1 //真正的这个月
  let firstDW = new Date(thisY, thisM, 1).getDay() //今天是周几
  let thisMD = monthDays[thisM] //现在这个月有几天
  let pastMD = firstDW //上个月显示几天
  let nextMD = 35 - thisMD - pastMD; //下个月显示几天
  let pastM = (thisM - 1) < 0 ? 11 : (thisM - 1) //判断上个月是不是去年12月
  let pastM_lastD = monthDays[pastM]; //判断上个月有几天

  let obj = [];
  for (let i = 0; i < pastMD; i++)
    obj.push({
      key: '',
      check:false,
      number: pastM_lastD - pastMD + i + 1
    })


  for (let i = 1; i <= thisMD; i++) {
    obj.push({
      key: thisY + '-' +( realM<10?'0'+realM:realM) + '-' +( i<10?'0'+i:i),
      check: false,
      number: i
    })
  }
  for (let i = 0; i < nextMD; i++)
    obj.push({
      key: '',
      check: false,
      number: i + 1
    })
  return obj;
}
let getMonth=function(){
  return month[thisM]
}

module.exports = {
  cal: cal,
  getMonth: getMonth
};