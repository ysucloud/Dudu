const test = require('test.js')
const http = require('../../utils/http.js')
Page({
  data: {
      sum:0,
      test : "今日未签到"
  },
  onLoad: function(options) {
    let calendar = test.cal()
    let that = this
    http.request({
      url: 'https://ksphp.erhuotuzi.cn/checkin/get',
      data: {
        'buyer_id': wx.getStorageSync('uid')
      },
      success: function(res) {
        console.log(res.data)
        that.setData({
          sum : res.data.length 
        })
        for (let i = 0; i < calendar.length; i++) {
          for (let j = 0; j < res.data.length; j++) {
            if (calendar[i].key == res.data[j].create_time) {
              calendar[i].check = true;
            }
          }
        }
        console.log(calendar)
        that.setData({
          calendar: calendar
        })
      }
    })
    http.request({
      url: 'https://ksphp.erhuotuzi.cn/checkin/is',
      data: {
        'buyer_id': wx.getStorageSync('uid')
      },
      success: function (res) {
        console.log(res.data)
        if(res.data.error_code == 1){
          that.setData({
            test:"今日已签到"
          })
        }
      }
    })
    this.setData({
      calendar: calendar,
      month: test.getMonth()
    })
  },
  onCheck: function() {
    var that = this
    http.request({
      url: 'https://ksphp.erhuotuzi.cn/checkin/save',
      data: {
        'buyer_id': wx.getStorageSync('uid')
      },
      success: function(res) {
        console.log(res.data)
        if(res.data.error_code == 0){
          that.onLoad()
        }
      }
    })
  }
})