// pages/pay/pay.js
const http = require('../../utils/http.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  pay(e) {
    wx.showLoading({
      title: '加载中...',
    })
    var url = getApp().globalData.php
    http.request({
      url: url + "pay/get",
      method: "POST",
      data: {
        "buyer_id": wx.getStorageSync("uid"),
        "order_id": this.data.id
      },
      success(e) {
        wx.hideLoading()
        wx.requestPayment({
          timeStamp: e.data.timeStamp,
          nonceStr: e.data.nonceStr,
          package: e.data.package,
          signType: e.data.signType,
          paySign: e.data.paySign,
          success(e) {
            wx.navigateTo({
              url: '../pay-result/pay-result?flag=' +"true" ,
            })
          },
          fail(e) {
            wx.navigateTo({
              url: '../pay-result/pay-result?flag=' + "false",
            })
          }
        })
      }
    })
  },
  getOrderDetail(id, callback) {
    var that = this
    var url = getApp().globalData.url
    wx.request({
      url: url + "/apis/order/detail",
      method: "POST",
      data: {
        "buyerId": wx.getStorageSync("uid"),
        "orderId": id
      },
      success(e) {
        callback(e.data.data)
      }
    })
  },
  onPayTap(e) {
    this.pay()
    // wx.navigateTo({
    //   url: '../after-pay/after-pay',
    // })
  },
  onLoad: function(e) {
    this.setData({
      id: e.orderId
    })
    var id = e.orderId
    this.getOrderDetail(id, (res) => {
      // console.log(res)
      this.setData({
        orderInfo: res
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})