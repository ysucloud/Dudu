// pages/second/second.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onFoodTap(e) {
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../home/detail/detail?id=' + id,
    })
  },
  getCommend(e) {
    var that = this
    var url = getApp().globalData.php
    wx.request({
      url: url + "new/get",
      method: "POST",
      success(e) {
        for (let i = 0; i < e.data.length; i++) {
          e.data[i].h = (e.data[i].product_price * 1.2).toFixed(2)
        }
        that.setData({
          commend: e.data
        })
      }
    })
  },
  onLoad: function (options) {
    this.getCommend()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})