package com.ysucloud.users.DTO;

import lombok.Data;

import java.util.Date;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 14:24
 */
@Data
public class BuyerDTO {
    private Integer id;

    /** 用户名（昵称） */
    private String username;
    /** 微信openid */
    private String openid;

    /** 自动时间戳 */
    private Date createTime;
    private Date updateTime;

    private String token;
}
