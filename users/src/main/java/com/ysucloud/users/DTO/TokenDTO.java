package com.ysucloud.users.DTO;

import lombok.Data;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/30 11:30
 */
@Data
public class TokenDTO {
    private String token;

    public TokenDTO(String token){
        this.token = token;
    }
}
