package com.ysucloud.users.controller;

import com.alibaba.fastjson.JSONObject;
import com.ysucloud.users.DTO.TokenDTO;
import com.ysucloud.users.VO.ResultObjectVO;
import com.ysucloud.users.exception.NullParameterException;
import com.ysucloud.users.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/30 11:06
 */
@RestController
@RequestMapping("/manager")
public class ManagerLoginController {
    @Autowired
    ManagerService managerService;

    @PostMapping("/login")
    public ResultObjectVO login(@RequestBody JSONObject jsonObject){
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");

        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            throw new NullParameterException();
        }

        TokenDTO token = managerService.login(username,password);

        return new ResultObjectVO(token);
    }

    @PostMapping("/logout")
    public ResultObjectVO logout(@RequestBody JSONObject jsonObject){
        String token = jsonObject.getString("token");
        if(StringUtils.isEmpty(token)){
            throw new NullParameterException();
        }

        managerService.logout(token);

        return new ResultObjectVO();
    }

}
