package com.ysucloud.users.controller;

import com.alibaba.fastjson.JSONObject;
import com.ysucloud.users.DTO.BuyerDTO;
import com.ysucloud.users.Utils.ResultUtil;
import com.ysucloud.users.VO.ResultObjectVO;
import com.ysucloud.users.exception.NullParameterException;
import com.ysucloud.users.service.BuyerService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 14:40
 */
@RestController
@RequestMapping("/buyer")
public class BuyerLoginController {
    @Autowired
    BuyerService buyerService;

    @PostMapping("/login")
    public ResultObjectVO login(@RequestBody JSONObject jsonObject){
        String code = jsonObject.getString("code");
        if(StringUtils.isEmpty(code)){
            throw new NullParameterException();
        }

        BuyerDTO buyerDTO = buyerService.login(code);

        return ResultUtil.Result(buyerDTO);
    }

    /** 废弃 */
    @PostMapping("/register")
    public ResultObjectVO register(@RequestBody JSONObject jsonObject){
        String code = jsonObject.getString("code");
        String name = jsonObject.getString("name");
        if(StringUtils.isEmpty(code)||StringUtils.isEmpty(name)){
            throw new NullParameterException();
        }

        BuyerDTO buyerDTO = buyerService.register(code,name);

        return ResultUtil.Result(buyerDTO);
    }
}
