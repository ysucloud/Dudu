package com.ysucloud.users.handler;

import com.ysucloud.users.VO.ResultObjectVO;
import com.ysucloud.users.exception.wxException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 13:21
 */
@RestController
@ControllerAdvice
public class wxHandler  {

    @ExceptionHandler(value = wxException.class)
    public ResultObjectVO NullSelectHandler(wxException e){
        ResultObjectVO result = new ResultObjectVO();
        result.setCode(10002);
        result.setMsg(e.getMsg());

        return result;
    }

}
