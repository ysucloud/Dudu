package com.ysucloud.users.handler;

import com.ysucloud.users.VO.ResultObjectVO;
import com.ysucloud.users.exception.FindUserException;
import com.ysucloud.users.exception.ManagerLoginException;
import com.ysucloud.users.exception.NotUserException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 15:01
 */
@RestController
@ControllerAdvice
public class UserExceptionHandler {
    /**
     * 登录用户不存在
     */
    @ExceptionHandler(value = NotUserException.class)
    public ResultObjectVO NotUserHandler(RuntimeException e) {
        ResultObjectVO result = new ResultObjectVO();
        result.setCode(10003);
        result.setMsg("尚未注册！");

        return result;
    }

    /**
     * 已经注册拦截
     */
    @ExceptionHandler(value = FindUserException.class)
    public ResultObjectVO FindUserHandler(RuntimeException e) {
        ResultObjectVO result = new ResultObjectVO();
        result.setCode(10003);
        result.setMsg("已经注册过！");

        return result;
    }

    /** 后台管理登录错误 */
    @ExceptionHandler(value = ManagerLoginException.class)
    public ResultObjectVO ManagerLoginHandler(RuntimeException e) {
        ResultObjectVO result = new ResultObjectVO();
        result.setCode(10003);
        result.setMsg("管理员账户密码输入错误！");

        return result;
    }

}
