package com.ysucloud.users.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 9:18
 */
@Entity
@Data
@Table(name = "buyer_info")
@DynamicUpdate
public class BuyerInfo {
    /** 用户主键（id） */
    @Id
    @GeneratedValue
    private Integer id;

    /** 用户名（昵称） */
    private String username;
    /** 微信openid */
    private String openid;

    /** 自动时间戳 */
    private Date createTime;
    private Date updateTime;

}
