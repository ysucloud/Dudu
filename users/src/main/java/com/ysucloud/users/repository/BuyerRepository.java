package com.ysucloud.users.repository;

import com.ysucloud.users.dataobject.BuyerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 10:14
 */
public interface BuyerRepository extends JpaRepository<BuyerInfo,Integer> {

    BuyerInfo findByOpenid(String openid);
}
