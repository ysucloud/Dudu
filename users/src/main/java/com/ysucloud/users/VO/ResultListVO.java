package com.ysucloud.users.VO;

import com.ysucloud.users.enums.ResultCode;
import lombok.Data;

import java.util.List;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 13:26
 */
@Data
public class ResultListVO<T> {
    private Integer Code = ResultCode.SUCCESS.getCode();
    private String Msg = ResultCode.SUCCESS.getMsg();

    List<T> data;
}
