package com.ysucloud.users.service;

import com.ysucloud.users.DTO.TokenDTO;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/30 11:16
 */
public interface ManagerService {

    TokenDTO login(String username, String password);

    void logout(String token);

}
