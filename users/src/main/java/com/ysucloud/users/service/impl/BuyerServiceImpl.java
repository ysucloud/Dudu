package com.ysucloud.users.service.impl;

import com.alibaba.fastjson.JSON;
import com.ysucloud.users.DTO.BuyerDTO;
import com.ysucloud.users.Utils.RedisUtil;
import com.ysucloud.users.Utils.TokenUtil;
import com.ysucloud.users.dataobject.BuyerInfo;
import com.ysucloud.users.exception.FindUserException;
import com.ysucloud.users.exception.NotUserException;
import com.ysucloud.users.repository.BuyerRepository;
import com.ysucloud.users.service.BuyerService;
import com.ysucloud.users.service.LoginService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 13:46
 */
@Service
public class BuyerServiceImpl implements BuyerService {
    @Autowired
    LoginService loginService;

    @Autowired
    BuyerRepository buyerRepository;

    @Autowired
    TokenUtil tokenUtil;

    @Autowired
    RedisUtil redisUtil;

    @Value("${WX.time}")
    Integer time;


    @Override
    public BuyerDTO login(String code) {
        /** 向微信官方获取openid */
        String openid = loginService.getOpenId(code);

        /** 查询数据库 */
        BuyerInfo buyerInfo = null;

        buyerInfo = buyerRepository.findByOpenid(openid);

        if(buyerInfo == null){
            //throw new NotUserException();
            BuyerDTO buyerDTO = this.registerbyopenid(openid);

            return buyerDTO;
        }

        /** token生成存储Redis */
        String token = tokenUtil.create();

        redisUtil.set(token, JSON.toJSONString(buyerInfo), time);

        /** 返回登录信息 */

        BuyerDTO buyerDTO = new BuyerDTO();
        BeanUtils.copyProperties(buyerInfo,buyerDTO);
        buyerDTO.setToken(token);

        return buyerDTO;
    }

    @Override
    public BuyerDTO register(String code,String name) {
        String openid = loginService.getOpenId(code);

        BuyerInfo buyerInfo = null;

        buyerInfo = buyerRepository.findByOpenid(openid);
        if(buyerInfo != null){
            throw  new FindUserException();
        }

        BuyerInfo newBuyer = new BuyerInfo();
        newBuyer.setUsername(name);
        newBuyer.setOpenid(openid);

        buyerRepository.save(newBuyer);

        /** token生成存储Redis */
        String token = tokenUtil.create();

        redisUtil.set(token, JSON.toJSONString(newBuyer), time);

        /** 返回登录信息 */

        BuyerDTO buyerDTO = new BuyerDTO();
        BeanUtils.copyProperties(newBuyer,buyerDTO);
        buyerDTO.setToken(token);

        return buyerDTO;
    }

    /** 通过openid注册 */
    private BuyerDTO registerbyopenid(String openid) {

        BuyerInfo newBuyer = new BuyerInfo();
        newBuyer.setUsername(openid);
        newBuyer.setOpenid(openid);

        buyerRepository.save(newBuyer);

        /** token生成存储Redis */
        String token = tokenUtil.create();

        redisUtil.set(token, JSON.toJSONString(newBuyer), time);

        /** 返回登录信息 */

        BuyerDTO buyerDTO = new BuyerDTO();
        BeanUtils.copyProperties(newBuyer,buyerDTO);
        buyerDTO.setToken(token);

        return buyerDTO;
    }
}
