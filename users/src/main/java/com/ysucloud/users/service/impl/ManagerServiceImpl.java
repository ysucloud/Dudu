package com.ysucloud.users.service.impl;

import com.ysucloud.users.DTO.TokenDTO;
import com.ysucloud.users.Utils.RedisUtil;
import com.ysucloud.users.Utils.TokenUtil;
import com.ysucloud.users.exception.ManagerLoginException;
import com.ysucloud.users.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/30 11:17
 */
@Service
public class ManagerServiceImpl implements ManagerService {
    @Value("${manager.username}")
    private String true_username;

    @Value("${manager.password}")
    private String true_password;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    TokenUtil tokenUtil;

    @Override
    public TokenDTO login(String username, String password) {
        if(!username.equals(true_username) || !password.equals(true_password)){
            throw new ManagerLoginException();
        }

        String token = tokenUtil.create();

        redisUtil.set(token,username,7200);

        return new TokenDTO(token);
    }

    @Override
    public void logout(String token) {
        redisUtil.del(token);
    }
}
