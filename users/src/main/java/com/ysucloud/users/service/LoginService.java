package com.ysucloud.users.service;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 13:05
 */
public interface LoginService {
    String getOpenId(String code);
}
