package com.ysucloud.users.service;

import com.ysucloud.users.DTO.BuyerDTO;
import com.ysucloud.users.dataobject.BuyerInfo;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 10:16
 */
public interface BuyerService {

    BuyerDTO login(String code);

    BuyerDTO register(String code,String name);
}
