package com.ysucloud.users.exception;

import lombok.Data;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 13:19
 */
@Data
public class wxException extends RuntimeException {

    private String msg;

    public wxException(String msg){
        this.msg = msg;
    }
}
