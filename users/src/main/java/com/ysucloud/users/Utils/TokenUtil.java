package com.ysucloud.users.Utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import sun.security.provider.MD5;

import java.util.Random;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 11:03
 */
@Component
public class TokenUtil {

    @Value("${salt}")
    private String salt;

    public String create(){
        Random random = new Random();
        Integer number = random.nextInt(9000) + 1000;

        String token = System.currentTimeMillis() + String.valueOf(number)+salt;
        token = DigestUtils.md5DigestAsHex(token.getBytes());

        return token;
    }
}
