package com.ysucloud.users.Utils;

import com.ysucloud.users.VO.ResultObjectVO;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 14:44
 */
public class ResultUtil {
    public static ResultObjectVO Result(Object object){
        ResultObjectVO result = new ResultObjectVO();

        result.setData(object);
        return result;
    }
}
