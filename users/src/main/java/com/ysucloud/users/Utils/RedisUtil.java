package com.ysucloud.users.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Description: users
 * Created by 刘翰文 on 2019/6/28 10:23
 */
@Component
public class RedisUtil {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    public boolean set(String key,String value,Integer time){
        stringRedisTemplate.opsForValue().set(
                key,
                value,
                time,
                TimeUnit.SECONDS
        );

        return true;
    }

    public String get(String key){
        String result = stringRedisTemplate.opsForValue().get(key);
        return result;
    }

    public boolean del(String key){
        stringRedisTemplate.delete(key);
        return true;
    }
}
