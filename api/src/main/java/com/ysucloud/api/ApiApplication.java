package com.ysucloud.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@SpringBootApplication
//@EnableDiscoveryClient
@EnableCircuitBreaker
//@SpringCloudApplication
public class ApiApplication
{
    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }

}
