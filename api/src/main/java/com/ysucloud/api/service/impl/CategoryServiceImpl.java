package com.ysucloud.api.service.impl;

import com.ysucloud.api.dataobject.ProductCategory;
import com.ysucloud.api.repository.ProductCategoryRepository;
import com.ysucloud.api.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 18:46
 */
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private ProductCategoryRepository productCategoryRepository;
    @Override
    public List<ProductCategory> findByCategoryIdIn(List<Integer> categoryIdList) {
        return productCategoryRepository.findByCategoryIdIn(categoryIdList);
    }
}
