package com.ysucloud.api.service;

import com.ysucloud.api.dataobject.ProductCategory;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 15:55
 */
public interface CategoryService
{
    /** 按照类目列表查询 */
    List<ProductCategory> findByCategoryIdIn(List<Integer> categoryIdList);
}
