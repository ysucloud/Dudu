package com.ysucloud.api.service;

import com.ysucloud.api.DTO.CartDTO;
import com.ysucloud.api.dataobject.ProductInfo;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 16:07
 */
public interface ProductService
{
    /** 查询所有在架商品列表 */
    List<ProductInfo> findUpAll();

    /** 按商品ID列表查询 */
    List<ProductInfo> findList(List<Integer> productIdList);

    /** 减库存 */
    void decreaseStock(List<CartDTO> cartDTOList);
}
