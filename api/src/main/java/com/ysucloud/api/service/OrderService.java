package com.ysucloud.api.service;

import com.ysucloud.api.DTO.OrderDTO;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 15:58
 */
public interface OrderService
{
    //创建订单
    OrderDTO create(OrderDTO orderDTO);
    //查询单个订单(停用)
    OrderDTO findOne(Integer orderId);
    //查询订单列表
    List<OrderDTO> findList(Integer buyerId);
    //取消订单
    //void cancel(Integer orderId);
    void cancel(Integer orderId,Integer buyerId);

    //查询单订单详情
    OrderDTO findDetail(Integer orderId,Integer buyerId);

}
