package com.ysucloud.api.handler;

import com.ysucloud.api.VO.ResultVO;
import com.ysucloud.api.exception.OrderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: api
 * Created by 刘翰文 on 2019/6/28 17:54
 */

@RestController
@ControllerAdvice
public class OrderExceptionHandler {

    @ExceptionHandler(value = OrderException.class)
    public ResultVO orderException(OrderException e){
        ResultVO result = new ResultVO();

        result.setCode(e.getCode());
        result.setMsg(e.getMsg());

        return result;
    }
}
