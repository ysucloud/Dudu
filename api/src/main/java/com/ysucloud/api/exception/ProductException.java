package com.ysucloud.api.exception;

import com.ysucloud.api.enums.ResultEnum;
import lombok.Data;

@Data
public class ProductException extends RuntimeException
{
    private Integer code;
    private String msg;

    public ProductException(Integer code, String message)
    {
        super(message);
        this.msg = message;
        this.code=code ;
    }
    public ProductException(ResultEnum resultEnum)
    {
        super(resultEnum.getMessage());
        this.msg = resultEnum.getMessage();
        this.code=resultEnum.getCode();
    }
}
