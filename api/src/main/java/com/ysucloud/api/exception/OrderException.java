package com.ysucloud.api.exception;


import com.ysucloud.api.enums.ResultEnum;
import lombok.Data;

@Data
public class OrderException extends RuntimeException
{
    private Integer code;
    private String msg;

    public OrderException(Integer code, String message)
    {
        this.msg = message;
        this.code=code;
    }

    public OrderException(ResultEnum resultEnum)
    {
        super(resultEnum.getMessage());
        this.code=resultEnum.getCode();
    }
}
