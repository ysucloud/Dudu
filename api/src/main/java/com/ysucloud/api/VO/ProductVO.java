package com.ysucloud.api.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProductVO
{
    @JsonProperty("name")
    private String categoryName;
    @JsonProperty("categoryId")
    private Integer categoryId;
    @JsonProperty("foods")
    private List<ProductInfoVO> productInfoVOList;
}
