package com.ysucloud.api.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/28 9:10
 */
@Data
public class OrderDetailVO {
    @JsonProperty("orderId")
    private Integer orderId;
    @JsonProperty("buyerName")
    private String buyerName;
    @JsonProperty("buyerPhone")
    private String buyerPhone;
    @JsonProperty("buyerAddress")
    private String buyerAddress;
    @JsonProperty("buyerId")
    private Integer buyerId;
    @JsonProperty("orderAmount")
    private BigDecimal orderAmount;
    @JsonProperty("orderStatus")
    private Integer orderStatus;
    @JsonProperty("payStatus")
    private Integer payStatus;
    @JsonProperty("createTime")
    private Date createTime;
    @JsonProperty("updateTime")
    private Date updateTime;
    @JsonProperty("orderDetailList")
    private List<OrderDetailInfoVO> orderDetailList;
}
