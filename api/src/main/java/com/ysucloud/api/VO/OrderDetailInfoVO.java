package com.ysucloud.api.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Description: api
 * Created by aininot260 on 2019/6/28 9:56
 */
@Data
public class OrderDetailInfoVO {
    @JsonProperty("detailId")
    private Integer detailId;
    @JsonProperty("orderId")
    private Integer orderId;
    @JsonProperty("productId")
    private Integer productId;
    @JsonProperty("productName")
    private String productName;
    @JsonProperty("productPrice")
    private BigDecimal productPrice;
    @JsonProperty("productQuantity")
    private Integer productQuantity;
    @JsonProperty("productIcon")
    private String productIcon;
}
