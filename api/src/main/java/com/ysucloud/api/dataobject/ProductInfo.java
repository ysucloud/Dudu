package com.ysucloud.api.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Description: manager
 * Created by 刘翰文 on 2019/6/27 9:18
 */
@Entity
@Data
@DynamicUpdate
public class ProductInfo {

    @Id
    @GeneratedValue
    private Integer productId;

    /** 名字 */
    private String productName;

    /** 单价 */
    private BigDecimal productPrice;

    /** 库存 */
    private Integer productStock;

    /** 描述 */
    private String productDescription;

    /** 小图 */
    private String productIcon;

    /** 状态, 1（true）正常0（false）下架 */
    private Integer productStatus;

    /** 类目编号 */
    private Integer categoryId;

    private Date createTime;

    private Date updateTime;


}
