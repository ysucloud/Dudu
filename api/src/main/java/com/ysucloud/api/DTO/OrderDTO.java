package com.ysucloud.api.DTO;

import com.ysucloud.api.dataobject.OrderDetail;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 15:47
 */
@Data
public class OrderDTO
{
    private Integer orderId;
    private String buyerName;
    private String buyerPhone;
    private String buyerAddress;
    private Integer buyerId;
    private BigDecimal orderAmount;
    private Integer orderStatus;
    private Integer payStatus;
    private Date createTime;
    private Date updateTime;
    private List<OrderDetail> orderDetailList;
}
