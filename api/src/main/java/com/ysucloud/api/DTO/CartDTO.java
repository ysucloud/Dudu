package com.ysucloud.api.DTO;

import lombok.Data;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 16:08
 */
@Data
public class CartDTO
{
    private Integer productId;
    private Integer productQuantity;

    public CartDTO(Integer productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
