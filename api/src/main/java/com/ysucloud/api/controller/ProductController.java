package com.ysucloud.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.ysucloud.api.Utils.ResultVOUtil;
import com.ysucloud.api.VO.ProductInfoVO;
import com.ysucloud.api.VO.ProductVO;
import com.ysucloud.api.VO.ResultVO;
import com.ysucloud.api.dataobject.ProductCategory;
import com.ysucloud.api.dataobject.ProductInfo;
import com.ysucloud.api.exception.NullParameterException;
import com.ysucloud.api.repository.ProductInfoRepository;
import com.ysucloud.api.service.CategoryService;
import com.ysucloud.api.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 16:22
 */
@RestController
@RequestMapping("/product")
public class ProductController
{
    @Autowired
    ProductService productService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ProductInfoRepository productInfoRepository;

    /** 查询列表 */
    @PostMapping("/list")
    public ResultVO<ProductVO> list()
    {
        //查询商品列表
        List<ProductInfo> productInfoList = productService.findUpAll();
        //将商品的品类ID列成List
        List<Integer> categoryIdList = productInfoList.stream()
                .map(ProductInfo::getCategoryId)
                .collect(Collectors.toList());
        //按照品类ID查询
        List<ProductCategory> categoryList = categoryService.findByCategoryIdIn(categoryIdList);
        //品类VO列表
        List<ProductVO> productVOList = new ArrayList<>();
        //遍历品类列表
        for(ProductCategory productCategory: categoryList)
        {
            //设置品类属性
            ProductVO productVO=new ProductVO();
            productVO.setCategoryName(productCategory.getCategoryName());
            productVO.setCategoryId(productCategory.getCategoryId());

            //具体商品List
            List<ProductInfoVO> productInfoVOList = new ArrayList<>();
            for(ProductInfo productInfo :productInfoList)
            {
                if(productInfo.getCategoryId()==productCategory.getCategoryId())
                {
                    //复制商品信息
                    ProductInfoVO productInfoVO=new ProductInfoVO();
                    BeanUtils.copyProperties(productInfo,productInfoVO);
                    productInfoVOList.add(productInfoVO);
                }
            }
            productVO.setProductInfoVOList(productInfoVOList);
            productVOList.add(productVO);
        }


        return ResultVOUtil.success(productVOList);
    }

    /** 获取单个商品详情 */
    @PostMapping("/get")
    public ResultVO get(@RequestBody JSONObject jsonObject){
        Integer id = jsonObject.getInteger("id");

        if(id == null){
            throw new NullParameterException();
        }

        ProductInfo productInfo = productInfoRepository.findById(id).get();

        return ResultVOUtil.success(productInfo);
    }


}
