package com.ysucloud.api.repository;

import com.ysucloud.api.dataobject.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 15:17
 */
public interface ProductCategoryRepository extends JpaRepository<ProductCategory,Integer>
{
    List<ProductCategory> findByCategoryIdIn(List<Integer> categoryIdList);
}
