package com.ysucloud.api.repository;

import com.ysucloud.api.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Description: api
 * Created by aininot260 on 2019/6/27 15:14
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail,Integer>
{
    List<OrderDetail> findByOrderId(Integer orderId);
}
