package com.ysucloud.api.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Description: api
 * Created by 刘翰文 on 2019/6/27 23:21
 */
@Data
public class OrderItemForm {

    @NotNull
    private Integer productId;

    @NotNull(message = "商品数量不能为空")
    private Integer productQuantity;

}
