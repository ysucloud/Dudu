package com.ysucloud.api.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


@Data
public class OrderForm
{
    @NotEmpty(message = "姓名必填")
    private String name;

    @NotEmpty(message="手机号必填")
    private String phone;

    @NotEmpty(message="地址必填")
    private String address;

    @NotNull
    private Integer buyerId;

    @Valid//递归校验
    @NotEmpty(message="购物车不能为空")
    private List<OrderItemForm> items;
}
