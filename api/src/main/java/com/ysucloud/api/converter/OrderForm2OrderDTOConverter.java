package com.ysucloud.api.converter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.ysucloud.api.DTO.OrderDTO;
import com.ysucloud.api.dataobject.OrderDetail;
import com.ysucloud.api.enums.ResultEnum;
import com.ysucloud.api.exception.OrderException;
import com.ysucloud.api.form.OrderForm;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class OrderForm2OrderDTOConverter
{
    /** OrderForm 转换 OrderDTO */
    public static OrderDTO convert(OrderForm orderForm)
    {
        OrderDTO orderDTO=new OrderDTO();
        orderDTO.setBuyerName(orderForm.getName());
        orderDTO.setBuyerPhone(orderForm.getPhone());
        orderDTO.setBuyerAddress(orderForm.getAddress());
        orderDTO.setBuyerId((orderForm.getBuyerId()));

        List<OrderDetail> orderDetailList=new ArrayList<>();
        String items = JSON.toJSONString(orderForm.getItems());
        try
        {
            orderDetailList = JSON.parseObject(items,
                    new TypeReference<List<OrderDetail>>(){});
        }
        catch (Exception e)
        {
            log.error("【json转换错误】，string={}",orderForm.getItems());
            throw new OrderException(ResultEnum.PARAM_ERROR);
        }
        orderDTO.setOrderDetailList(orderDetailList);
        return orderDTO;
    }
}
