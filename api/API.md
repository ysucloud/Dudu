# API

###商品列表//完成代码检查 BUG:1

```
POST /product/list
```

参数

```
无
```

返回

```
{
  "code": 0,
  "msg": "成功",
  "data": [
    {
      "name": "牧产品",
      "categoryId": 2,
      "foods": [
        {
          "productId": "2",
          "name": "香菇肥牛",
          "price": 0.02,
          "description": "肥嫩多汁",
          "icon": "https://timgsa.bai`****`du.com/timg?image&quality=80&size=b9999_10000&sec=1562245655&di=e01f52d69ae420d7d2b6b2334a1247c5&imgtype=jpg&er=1&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fq_70%2Cc_zoom%2Cw_640%2Fimages%2F20180419%2F9bc77ca7099345cfac6066371a5393c6.jpeg"
        }
      ]
    },
    {
      "name": "海产品",
      "categoryId": 3,
      "foods": [
        {
          "productId": "3",
          "name": "油炸象拔蚌",
          "price": 0.01,
          "description": "好吃不贵",
          "icon": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1561650957619&di=e92442f6ca121ec752626ff89a674e01&imgtype=0&src=http%3A%2F%2Fimg02.tooopen.com%2Fimages%2F20151203%2Ftooopen_sy_150243552595.jpg"
        }
      ]
    }
  ]
}
```


###创建订单//完成代码检查  ERROR：1 BUG：2

```
POST /order/create
```

参数

```
{
	"name": "龚昊",
	"phone": "13832290486",
	"address": "燕山大学",
	"buyerId": 1,
	"items": [{
		"productId": 1,
		"productQuantity": 2
	}]
}

```

返回

```
{
  "code": 0,
  "msg": "成功",
  "data": {
      "orderId": 7
  }
}
```

###订单列表  //完成代码检查  BUG:1

```
GET /order/list
```

参数

```
{
	"buyerId": 1
}
```

返回

```
{
  "code": 0,
  "msg": "成功",
  "data": [
    {
      "orderId": 6,
      "buyerName": "龚昊",
      "buyerPhone": "13832290486",
      "buyerAddress": "燕山大学",
      "buyerId": 1,
      "orderAmount": 21.8,
      "orderStatus": 0,
      "payStatus": 0,
      "createTime": "2019-06-28 08:54:07",
      "updateTime": "2019-06-28 08:54:39"
    },
    {
      "orderId": 7,
      "buyerName": "龚昊",
      "buyerPhone": "13832290486",
      "buyerAddress": "燕山大学",
      "buyerId": 1,
      "orderAmount": 21.8,
      "orderStatus": 0,
      "payStatus": 0,
      "createTime": "2019-06-28 09:05:05",
      "updateTime": "2019-06-28 09:05:06"
    }
  ]
}
```

###查询订单详情  （重写）

```
POST /order/detail
```

参数

```
{
	"buyerId": 1,
	"orderId": 8
}
```

返回

```
{
  "code": 0,
  "msg": "成功",
  "data": {
    "orderId": 8,
    "buyerName": "龚昊",
    "buyerPhone": "13832290486",
    "buyerAddress": "燕山大学",
    "buyerId": 1,
    "orderAmount": 21.86,
    "orderStatus": 0,
    "payStatus": 0,
    "createTime": "2019-06-28 10:11:13",
    "updateTime": "2019-06-28 10:11:14",
    "orderDetailList": [
      {
        "detailId": 9,
        "orderId": 8,
        "productId": 1,
        "productName": "金针菇",
        "productPrice": 10.9,
        "productQuantity": 2,
        "productIcon": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1561650911950&di=e6c25fb9e6b49dab9bc8556b9b763e49&imgtype=0&src=http%3A%2F%2Fimages4.c-ctrip.com%2Ftarget%2Ffd%2Ftuangou%2Fg2%2FM0A%2FF1%2F77%2FCghzgFUBF7WAERQsAAOHW_LiU0U830_720_480_s.jpg"
      },
      {
        "detailId": 10,
        "orderId": 8,
        "productId": 2,
        "productName": "香菇肥牛",
        "productPrice": 0.02,
        "productQuantity": 3,
        "productIcon": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1562245655&di=e01f52d69ae420d7d2b6b2334a1247c5&imgtype=jpg&er=1&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fq_70%2Cc_zoom%2Cw_640%2Fimages%2F20180419%2F9bc77ca7099345cfac6066371a5393c6.jpeg"
      }
    ]
  }
}
```

###取消订单  （重写）

```
POST /order/cancel
```

参数

```
{
	"buyerId": 1,
	"orderId": 8
}
```

返回

```
{
    "code": 0,
    "msg": "成功",
    "data": null
}
```



