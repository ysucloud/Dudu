import fetch from '@/config/fetch'

/**
 * 登陆1
 */

export const login = data => fetch('/users/manager/login', data, 'POST');

/**
 * 退出1
 */

export const signout = () => fetch('/admin/signout');

/**
 * 获取用户信息1
 */

export const getAdminInfo = () => fetch('/admin/info');

/**
 * api请求量0
 */

export const apiCount = date => fetch('/statis/api/' + date + '/count');

/**
 * 所有api请求量0
 */

export const apiAllCount = () => fetch('/statis/api/count');


/**
 * 所有api请求信息0
 */

export const apiAllRecord = () => fetch('/statis/api/all');

/**
 * 用户注册量1
 */

export const userCount = () => fetch('/manager/user/count');

/**
 * 某一天订单数量1
 */

export const orderCount = () => fetch('/manager/order/count');


export const orderWeek = () => fetch('/manager/order/week');

export const userWeek = () => fetch('/manager/user/week');


/**
 * 某一天管理员注册量0
 */

export const adminDayCount = date => fetch('/statis/admin/' + date + '/count');

/**
 * 管理员列表0
 */

export const adminList = data => fetch('/admin/all', data);

/**
 * 管理员数量0
 */

export const adminCount = () => fetch('/admin/count');

/**
 * 获取定位城市1
 */

export const cityGuess = () => fetch('/v1/cities', {
	type: 'guess'
});

/**
 * 添加商铺0
 */

export const addShop = data => fetch('/shopping/addShop', data, 'POST');

/**
 * 获取搜索地址0
 */

export const searchplace = (cityid, value) => fetch('/v1/pois', {
	type: 'search',
	city_id: cityid,
	keyword: value
});

/**
 * 获取当前店铺食品种类1
 */

export const getCategory = () => fetch('/manager/type/get',"POST");

/**
 * 添加食品种类1
 */

export const addCategory = data => fetch('/manager/type/save', data, 'POST');


/**
 * 添加食品1
 */

export const addFood = data => fetch('/manager/product/save', data, 'POST');


/**
 * category 种类列表1
 */

export const foodCategory = (latitude, longitude) => fetch('/shopping/v2/restaurant/category');

/**
 * 获取商店列表0
 */

export const getResturants = data => fetch('/shopping/restaurants', data);

/**
 * 获取商店详细信息0
 */

export const getResturantDetail = restaurant_id => fetch('/shopping/restaurant/' + restaurant_id);

/**
 * 获取商店数量0
 */

export const getResturantsCount = () => fetch('/shopping/restaurants/count');

/**
 * 更新商店信息0
 */

export const updateResturant = data => fetch('/shopping/updateshop', data, 'POST');

/**
 * 删除商店0
 */

export const deleteResturant = restaurant_id => fetch('/shopping/restaurant/' + restaurant_id, {}, 'DELETE');

/**
 * 获取食品列表1
 */

export const getFoods = () => fetch('/manager/product/get');

/**
 * 获取食品数量1
 */

export const getFoodsCount = data => fetch('/shopping/v2/foods/count', data);


/**
 * 获取menu列表0
 */

export const getMenu = data => fetch('/shopping/v2/menu', data);

/**
 * 获取menu详情0
 */

export const getMenuById = category_id => fetch('/shopping/v2/menu/' + category_id);

/**
 * 更新食品信息1
 */

export const updateFood = data => fetch('/manager/product/change', data, 'POST');

/**
 * 删除食品1
 */

export const deleteFood = food_id => fetch('/manager/product/delete' , food_id, 'POST');

/**
 * 获取用户列表1
 */

export const getUserList = data => fetch('/v1/users/list', data);

/**
 * 获取用户数量1
 */

export const getUserCount = data => fetch('/v1/users/count', data);

/**
 * 获取订单列表1
 */

export const getOrderList = () => fetch('/manager/order/list');

/**
 * 获取订单数量1
 */

export const getOrderCount = data => fetch('/bos/orders/count', data);

export const finishOrder = data => fetch('/manager/order/finish', data);

/**
 * 获取用户信息0
 */

export const getUserInfo = user_id => fetch('/v1/user/' + user_id);

/**
 * 获取地址信息0
 */

export const getAddressById = address_id => fetch('/v1/addresse/' + address_id);

/**
 * 获取用户分布信息0
 */

export const getUserCity = () => fetch('/v1/user/city/count');


//商家消息

export const addMessage = (data) => fetch('/exapis/message/save',data,'POST');