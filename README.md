# Dudu

#### 介绍
 管理后台基本代码   
提供管理后台基本增删改查功能，针对校验的部分放在Zuul网关层处理  

#### 主要部分
1、注册中心 Eureka   
2、服务网关zuul-gateway  
3、主要微服务 api users manager exapis  
4、统一配置中心 config  
5、PHP部分后台   
6、管理后台CMS VUE   

#### 软件架构  
软件架构说明（JAVA）   
config- 配置been   
controller- 控制层   
converter- 转换器   
dataobject- 数据表实例   
dto-中间数据表   
enums-特殊枚举类   
exception-自定义异常类   
form-AOP复杂数据校验层   
handler-异常拦截器   
repository-数据库接口（jpa）    
server-服务层接口   
Utils-自定义工具类   
VO-返回数据模型   
server.impl-服务层实现   

(PHP)    
controller- 控制层   
model- 模型层    
service- 服务层  

#### 编码说明  
JAVA    
基本采用驼峰方式命名  
以代码注释为准  
PHP    
基本采取下划线风格命名    
以代码注释为准    