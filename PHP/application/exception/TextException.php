<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/9/12
 * Time: 14:14
 */

namespace app\exception;


class TextException extends BaseExceptions
{
    public $code=200;
    public $msg='您的内容可能违反相关法律，不能被发表';
    public $errorcode=10010;
}