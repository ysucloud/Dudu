<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/7/16
 * Time: 10:21
 */

namespace app\exception;


class NotFindException extends BaseExceptions
{
    public $code = 200;
    public $msg="";
    public $errorcode = 10001;
}