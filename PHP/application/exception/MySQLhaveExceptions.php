<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/7/20
 * Time: 11:40
 */

namespace app\exception;


class MySQLhaveExceptions extends BaseExceptions
{
    public $code = 408;
    public $errorcode = 10006;
    public $msg= "数据库插入错误";
}