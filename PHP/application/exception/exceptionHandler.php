<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/7/16
 * Time: 8:58
 */

namespace app\exception;
//异常处理类
use Exception;
use think\exception\Handle;
use think\facade\Request;

class exceptionHandler extends Handle
{
    private $code;
    private $msg;
    private $errorcode;

    public function render(Exception $e){
        //return 0;
        if($e instanceof BaseExceptions){
            $this->code=$e->code;
            $this ->msg=$e->msg;
            $this->errorcode = $e->errorcode;
        }
        else{
            if(config('app_debug')){
                return parent::render($e);
            }
            else {
                $this->code = 500;
                $this->msg = "内部错误";
                $this->errorcode = 20000;
            }
        }

        //$request = Request::instance();

        $result =[
            'msg' => $this->msg,
            'errorcode' => $this->errorcode,
            'request_url' => Request::url(true)
        ];

        return json($result,$this->code);
    }
}