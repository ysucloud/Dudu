<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/7/16
 * Time: 10:35
 */

namespace app\exception;


class parameterException extends BaseExceptions
{
    public $code = 403;
    public $errorcode = 10002;
}