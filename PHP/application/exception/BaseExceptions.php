<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/7/16
 * Time: 10:15
 */

namespace app\exception;


use think\Exception;

class BaseExceptions extends Exception
{
    public $code;
    public $msg;
    public $errorcode=10000;
    public function __construct($params=[])
    {
        if(!is_array($params)){
            return;
        }
        if(array_key_exists('code',$params)){
            $this->code = $params['code'];
        }
        if(array_key_exists('msg',$params)){
            $this->msg = $params['msg'];
        }
        if(array_key_exists('errorCode',$params)){
            $this->errorcode = $params['errorCode'];
        }
    }
}