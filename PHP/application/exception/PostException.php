<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/8/1
 * Time: 18:40
 */

namespace app\exception;


class PostException extends BaseExceptions
{
    public $code = 409;
    public $errorcode = 10007;
}