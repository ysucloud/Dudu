<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/7/20
 * Time: 10:14
 */

namespace app\exception;


class OrderExceptions extends BaseExceptions
{
    public $code = 407;
    public $msg="";
    public $errorcode = 10005;
}