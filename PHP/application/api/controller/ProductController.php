<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/3
 * Time: 15:59
 */

namespace app\api\controller;


use app\api\model\ProductInfo;

class ProductController extends BaseControllers
{
    public function newList(){
        return ProductInfo::order('create_time desc')->select();
    }

    public function getSome(){
        return ProductInfo::orderRaw('rand()')->limit(10)->select();
    }



}