<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/7
 * Time: 12:49
 */

namespace app\api\controller;


use app\api\model\Comment;

class CommentController extends BaseControllers
{
    public function all($id){
        return Comment::where(['product_id' => $id])->with('info')->select();
    }

}