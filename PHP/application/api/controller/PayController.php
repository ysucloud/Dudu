<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/4
 * Time: 8:46
 */

namespace app\api\controller;

use app\api\model\BuyerInfo;
use app\api\model\OrderDetail;
use app\api\service\PayService;
use app\api\model\OrderMaster;
use app\exception\payExceptions;
use app\api\service\WxNotify;

class PayController extends BaseControllers
{
    public function pay($buyer_id,$order_id){
        $order = OrderMaster::where([
                'order_id'=>$order_id,
                'buyer_id'=>$buyer_id
            ])->find();

        $buyer = BuyerInfo::where(['id' => $buyer_id])->find();

        if($order == null || $buyer == null){
            throw new payExceptions(['msg' => '订单或者用户不存在！']);
        }
        $payService = new PayService();

        $wxresult = $payService->pay($order_id,$buyer['openid'],$order['order_amount'] * 100);

        return $wxresult;

        //return config('wx.mchid');
    }

    public function payNotify(){
        $notify=new WxNotify();

        $notify->Handle();
    }

    public function test($order_id){
        $bodys = OrderDetail::where(['order_id' => $order_id])->with('product')->select();

        $body = "";

        for($i = 0 ;$i<sizeof($bodys);$i++){
            $body = $body.$bodys[$i]['product']['product_name'];
        }

        return $body;
    }

}