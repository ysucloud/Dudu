<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/3
 * Time: 16:27
 */

namespace app\api\controller;

use app\api\model\CheckIn as CheckInModel;

class CheckInController extends BaseControllers
{
    public function checkIn($buyer_id){
        $before = date('Y-m-d 00:00:00',strtotime('now'));
        $after = date('Y-m-d 23:59:59',strtotime('now'));

        $result = CheckInModel::where(['buyer_id'=>$buyer_id])->whereBetweenTime('create_time',$before,$after)->select();

        if(sizeof($result)!=0){
            return json(['error_code' => 1]);
        }
        CheckInModel::create([
           'buyer_id'=>$buyer_id
        ]);

        return json(['error_code' => 0]);
    }

    public function getList($buyer_id){
        $before = date('Y-m-01',strtotime('now'));
        $after = date('Y-m-t',strtotime('now'));

        return CheckInModel::where(['buyer_id' => $buyer_id])->whereBetweenTime('create_time',$before,$after)->select();
    }

    public function isCheckIn($buyer_id){
        $before = date('Y-m-d 00:00:00',strtotime('now'));
        $after = date('Y-m-d 23:59:59',strtotime('now'));

        $result = CheckInModel::where(['buyer_id'=>$buyer_id])->whereBetweenTime('create_time',$before,$after)->select();

        if(sizeof($result) == 0){
            return json(['error_code' => 0]);
        }

        return json(['error_code' => 1]);
    }

}