<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/3
 * Time: 16:13
 */

namespace app\api\controller;


use app\api\model\Comment;
use app\api\model\OrderComment;
use app\api\model\OrderMaster;

class OrderController extends BaseControllers
{
    public function confirm($order_id){
        OrderMaster::update(['order_status'=>3],['order_id'=>$order_id]);

        return json(['error_code'=>0]);
    }

    public function comment($order_id,$info){
        OrderMaster::update(['order_status'=>1],['order_id'=>$order_id]);
        OrderComment::create([
            'order_id'=>$order_id,
            'comment'=>$info
        ]);

        $order = OrderMaster::where(['order_id' => $order_id])->with('OrderDetail')->find();

        Comment::create([
            'product_id' => $order['OrderDetail'][0]['product_id'],
            'buyer_id' => $order['buyer_id'],
            'message' => $info
        ]);

        return json(['error_code'=>0]);
    }

    public function order_list($buyerId){
        return OrderMaster::where(['buyer_id' => $buyerId])->where('order_status','in',[0,1,2,3])->with('OrderDetail')->select();
    }

    public function order_delete($order_id){
        OrderMaster::update([
            'order_status' => 4
        ],[
            'order_id' => $order_id
        ]);

        return json(['error_code'=>0]);
    }

}