<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/3
 * Time: 15:51
 */

namespace app\api\controller;

//秒杀

use app\api\model\SecondProduct;

class MxController extends BaseControllers
{
    public function get(){
        return SecondProduct::with('info')->select();
    }

}