<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/4/25
 * Time: 21:15
 */

namespace app\api\cors;

use think\facade\Request;

class CORS
{
    public function run()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: POST,GET');
        if (request()->isOptions()) {
            exit();
        }
    }
}