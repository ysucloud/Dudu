<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/3
 * Time: 15:47
 */

namespace app\api\model;


class OrderDetail extends BaseModels
{
    public function product(){
        return $this->hasOne('ProductInfo','product_id','product_id');
    }

}