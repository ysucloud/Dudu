<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/3
 * Time: 16:28
 */

namespace app\api\model;


class CheckIn extends BaseModels
{
    protected $autoWriteTimestamp = 'datetime';


    public function getCreateTimeAttr($value){
        return mb_substr($value,0,10,'UTF-8');
    }
}