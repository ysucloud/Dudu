<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2019/7/3
 * Time: 15:43
 */

namespace app\api\model;


class BuyerInfo extends BaseModels
{
    public function info(){
        return $this->hasOne('BuyerInfos','buyer_id','id');
    }

}