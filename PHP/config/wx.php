<?php
/**
 * Created by PhpStorm.
 * User: 刘翰文
 * Date: 2018/7/11
 * Time: 21:35
 */

//微信小程序相关参数设置

return [
    'appid' => '小程序APPID',
    'appsecret' => '小程序APPSECRET',
    'loginurl' => "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code",
    'mchid'=>"微信商户号",
    'key'=> '微信商户KEY'
];