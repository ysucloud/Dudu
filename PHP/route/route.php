<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});

Route::get('hello/:name', 'index/hello');

Route::post('ms/get','api/MxController/get');

Route::post('new/get','api/ProductController/newList');
Route::post('tj/get','api/ProductController/getSome');

Route::post('order/confirm','api/OrderController/confirm');
Route::post('order/comment','api/OrderController/comment');
Route::post('order/list','api/OrderController/order_list');
Route::post('order/delete','api/OrderController/order_delete');

Route::post('checkin/get','api/CheckInController/getList');
Route::post('checkin/save','api/CheckInController/checkIn');
Route::post('checkin/is','api/CheckInController/isCheckIn');

Route::post('pay/get','api/PayController/pay');
Route::post('pay/test','api/PayController/test');
Route::post('pay/no','api/PayController/payNotify');

Route::post('comment/list','api/CommentController/all');

return [

];
